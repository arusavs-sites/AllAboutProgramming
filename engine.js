var site = {
  title: 'Все о программировании',
  description: 'Краткая информация о современных технологиях разработки программного обеспечения',
  url: 'http://все-о-программировании.рф/',
  pages: {
    about: {
      url: 'pages/about.html',
      title: 'О сайте'
    }
  },

  content: {
    algorithms: {
      url: 'articles/algorithms/sorting.html',
      title: 'Алгоритмы сортировки',
      description: ''
    }
  }
};

if (typeof window === 'undefined') {
  // run in nodejs - update rss and sitemap
  updateSite();
} else {
  // run in browser
  window.onload = function() {

    function addLinks(ulElementId, listObject) {
      var ul = document.getElementById(ulElementId);
      if (ul != null) {
        for (var itemName in listObject) {
          if (listObject.hasOwnProperty(itemName)) {
            var item = listObject[itemName];
            var li = document.createElement("li");
            var a = document.createElement("a");
            var text = document.createTextNode(item.title);
            li.appendChild(a);
            a.appendChild(text);
            a.href = "/" + item.url;
            ul.insertBefore(li, ul.firstChild);
          }
        }
      }
    }

    addLinks("pages", site.pages);
    //addLinks("content", site.content);
  };
}


function updateSite() {

  var fs = require("fs");

  // update rss
  var rss = `<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel>` +
    `<title>${site.title}</title>` +
    `<description>${site.description}</description>` +
    `<link>${site.url}</link><language>ru-ru</language>`;
  for (var name in site.content) {
    if (site.content.hasOwnProperty(name)) {
      var item = site.content[name];
      rss += `<item><title>${item.title}</title>` +
        `<description>${item.description}</description>` +
        `<link>${site.url}${item.url}</link></item>`;
    }
  }
  rss += `</channel></rss>`;
  fs.writeFile("rss.xml", rss, function(err) {
    if (err) {
      console.log("Error update RSS");
      return console.log(err);
    }
    console.log("RSS article updated!");
  });

  rss = `<?xml version="1.0" encoding="UTF-8" ?><rss version="2.0"><channel>` +
    `<title>${site.title} - Страницы</title>` +
    `<description>${site.description}</description>` +
    `<link>${site.url}</link><language>ru-ru</language>`;
  for (var name in site.pages) {
    if (site.pages.hasOwnProperty(name)) {
      var item = site.pages[name];
      rss += `<item><title>${item.title}</title>` +
        `<link>${site.url}${item.url}</link></item>`;
    }
  }
  rss += `</channel></rss>`;
  fs.writeFile("pages.xml", rss, function(err) {
    if (err) {
      console.log("Error update RSS");
      return console.log(err);
    }
    console.log("RSS pages updated!");
  });

  // updateSitemap
  var sitemap = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`;
  for (var name in site.content) {
    if (site.content.hasOwnProperty(name)) {
      var item = site.content[name];
      sitemap += `<url><loc>${site.url}${item.url}</loc></url>`;
    }
  }
  for (var name in site.pages) {
    if (site.pages.hasOwnProperty(name)) {
      var item = site.pages[name];
      sitemap += `<url><loc>${site.url}${item.url}</loc></url>`;
    }
  }
  sitemap += `</urlset>`;
  fs.writeFile("sitemap.xml", sitemap, function(err) {
    if (err) {
      console.log("Error update Sitemap");
      return console.log(err);
    }
    console.log("Sitemap updated!");
  });

};
